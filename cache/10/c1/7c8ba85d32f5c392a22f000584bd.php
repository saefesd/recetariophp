<?php

/* agregar.twig.html */
class __TwigTemplate_10c17c8ba85d32f5c392a22f000584bd extends Twig_Template
{
    protected $parent;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'subtitulo' => array($this, 'block_subtitulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    public function getParent(array $context)
    {
        if (null === $this->parent) {
            $this->parent = $this->env->loadTemplate("layout.twig.html");
        }

        return $this->parent;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_subtitulo($context, array $blocks = array())
    {
        // line 4
        echo "\t<a class=\"titulillo\">AGREGAR RECETA</a>
\t<br />
\tFormulario que recoge los datos de la nueva receta
";
    }

    // line 9
    public function block_contenido($context, array $blocks = array())
    {
        // line 10
        echo "\t<div class=\"usunew\" align=\"center\">
\t\t<p class=\"tit2\">RELLENA LOS DATOS DE LA NUEVA RECETA</p>
\t\t<p>Titulo : <input id=\"name\" name=\"titulo\" type=\"text\" /></p>
\t\t<p>Tipo : <select name=\"tipo\">
\t\t\t<option value=\"Primero\">Primero</option>
\t\t\t<option value=\"Segundo\">Segundo</option>
\t\t\t<option value=\"Entrante\">Entrante</option>
\t\t\t<option value=\"Postre\">Postre</option>
\t\t</select></p>
\t\t<p>Ingredientes : <input id=\"ingredientes\" name=\"ingredientes\" type=\"text\" /></p>
\t\t<p>Preparacion : <input id=\"preparacion\" name=\"preparacion\" type=\"text\" /></p>
\t\t<p>Imagen : <input id=\"imagen\" name=\"imagen\" type=\"file\" /></p>
\t\t<br />
\t\t<input type=\"submit\" value=\"Limpiar\" name=\"limpiar\"id=\"limpiar\"><input type=\"submit\" value=\"Aceptar\" name=\"aceptar\" id=\"aceptar\">
\t</div>
";
    }

    public function getTemplateName()
    {
        return "agregar.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }
}
