<?php

/* index.twig.html */
class __TwigTemplate_80c70b15188155457ba6d77628a8e747 extends Twig_Template
{
    protected $parent;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'subtitulo' => array($this, 'block_subtitulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    public function getParent(array $context)
    {
        if (null === $this->parent) {
            $this->parent = $this->env->loadTemplate("layout.twig.html");
        }

        return $this->parent;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_subtitulo($context, array $blocks = array())
    {
        // line 4
        echo "\t<a class=\"titulillo\">INICIO</a>
\t<br />
\tBienvenido al Recetario en PHP con plantillas TWIG
";
    }

    // line 9
    public function block_contenido($context, array $blocks = array())
    {
        // line 10
        echo "\t<p>
\t\tEsta es mi tarea de Recetario en <a href=\"http://www.php.net/manual/es/\" class=\"vinculo\">PHP</a> 
\t\tpara <span class=\"imp1\">Desarrollo de Aplicaciones Web</span>  en 
\t\t<a href =\"http://www.cpilosenlaces.com/cpilosenlaces/cpifp_los_enlaces_inicio/cpifp_los_enlaces_inicio.html\" class=\"vinculo\">
\t\t\tLos Enlaces
\t\t</a>. <br /><br />
\t\tPara realizar el trabajo he utilizado el motor de plantillas <span class=\"imp2\">Twig</span>.
\t\tPuedes consultar su documentación <a class=\"vinculo\" href=\"http://gitnacho.github.io/Twig/intro.html\">aquí</a>. El Ide con el
\t\tque he trabajado ha sido <span class=\"imp2\">Aptana Studio 3</span>.
\t</p>
\t<p>
\t\tLa aplicación web lista las recetas guardadas en una base de datos <span class=\"imp2\">sqlite</span> en el menú <span class=\"imp1\">
\t\t\tListado</span> Desde el menú <span class=\"imp1\">Agregar Receta</span> podremos añadir nuevas recetas a nuestra base de datos
\t\t\tademás de editar su información asi como borrarlas.<br />
\t\t\tDesde el menu <span class=\"imp1\">Comentar receta</span> podremos añadir comentarios a esas recetas.
\t\t\t<br />
\t\t\tEn el menú <span class=\"imp1\">Usuarios</span> veremos los usuarios que se han creado en la base de datos y podremos crear nuevos 
\t\t\tusuarios.
\t\t</p>
\t\t<p>Bon Apetit.</p>
";
    }

    public function getTemplateName()
    {
        return "index.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }
}
