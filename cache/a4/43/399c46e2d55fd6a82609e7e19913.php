<?php

/* layout.twig.html */
class __TwigTemplate_a443399c46e2d55fd6a82609e7e19913 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'subtitulo' => array($this, 'block_subtitulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"> 
<html xmlns=\"http://www.w3.org/1999/xhtml\"> 
  <head>
    <title>";
        // line 4
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    <link href='http://fonts.googleapis.com/css?family=Noto Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'/>
\t<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'/>
\t<meta charset=\"utf8\" />
\t<link href=\"css/estilos.css\" rel=\"stylesheet\" />
  </head>
  <body>
  \t<div class=\"cabecera\" align=\"center\">
  \t\t<img src=\"images/recetario.jpg\" class=\"imgheader\" />
  <br />
   ";
        // line 14
        if ($this->getAttribute($this->getContext($context, 'user'), "is_authenticated", array(), "any", false)) {
            // line 15
            echo "          
         \t<a href=\"/cerrar\" class=\"login\">Cerrar sesion</a>
         \t<a href=\"/perfil\" class=\"login\">Perfil</a>
         
         ";
        } else {
            // line 20
            echo "          <a href=\"login.php\" class=\"login\">Iniciar sesion</a>
          \t<a href=\"usuarios.php\" class=\"login\">Registrarse</a>
          \t
          ";
        }
        // line 24
        echo "  \t\t
    <h1>RECETARIO REALIZADO POR MARCO A. SANCHEZ</h1>
    </div>
\t<div class=\"menu\">
\t\t<ul>
\t\t\t<li><a href=\"index.php\">INICIO</a></li>
\t\t\t<li><a href=\"listado.php\">LISTADO</a></li>
\t\t\t<li><a href=\"agregar.php\">AGREGAR RECETA</a></li>
\t\t\t<li><a href=\"comentar.php\">COMENTAR RECETA</a></li>
\t\t\t<li><a href=\"usuarios.php\">USUARIOS</a></li>
\t\t</ul>
\t</div>
\t<div class=\"subtitulo\">
\t<h2>";
        // line 37
        $this->displayBlock('subtitulo', $context, $blocks);
        echo "</h2>
\t</div>
    <div class=\"contenido\">
      ";
        // line 40
        $this->displayBlock('contenido', $context, $blocks);
        // line 42
        echo "    </div>
    <div class=\"pie\">
    \t<a class=\"piecnt\" href=\"https://mail.google.com/mail/u/0/?tab=Xm#inbox?compose=14419d4a52d79df5\">Contactar</a> <a class=\"piecnt\" href=\"https://bitbucket.org/saefesd/recetariophp\">Repositorio</a>
    <a class=\"piecnt\" href=\"https://www.google.es/#q=recetas+de+cocina\">Recetas en Google</a>
    <a class=\"piecnt\" href=\"https://www.youtube.com/results?search_query=recetas%20de%20cocina&sm=3\">Recetas en Youtube</a>
    <a class=\"piecnt\" href=\"http://jhernandz.es/noticia/twig-plantillas-php-i\">Tutorial Twig</a>
    \t <a class=\"piecnt\" href=\"http://www.saefesd.infenlaces.com/trabajos/twig.pdf\">Manual Twig pdf</a>
    \t
    
    </div>
    <div class=\"pie2\">
    \t<img src=\"images/php.png\" class=\"img1\"/>
    \t<img src=\"images/sqlite.png\" class=\"img1\"/>
    \t<img src=\"images/twig.png\" class=\"img1\"/>
    \t<img src=\"images/aptana.png\" class=\"img1\"/>
    \t<img src=\"images/pdf.png\" class=\"img1\"/>
    \t<img src=\"images/bitbucket.png\" class=\"img2\"/>
    </div>
    <div class=\"pie3\" align=\"center\">
   <a class=\"piecnt2\">D.A.W. 2º - @saefesd 2014</a>
    </div>
  </body>
</html>
";
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        echo "Recetario PHP";
    }

    // line 37
    public function block_subtitulo($context, array $blocks = array())
    {
    }

    // line 40
    public function block_contenido($context, array $blocks = array())
    {
        // line 41
        echo "      ";
    }

    public function getTemplateName()
    {
        return "layout.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }
}
