<?php

/* listado.twig.html */
class __TwigTemplate_46a2740a20ecc6f80c965706bd43eb90 extends Twig_Template
{
    protected $parent;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'subtitulo' => array($this, 'block_subtitulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    public function getParent(array $context)
    {
        if (null === $this->parent) {
            $this->parent = $this->env->loadTemplate("layout.twig.html");
        }

        return $this->parent;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_subtitulo($context, array $blocks = array())
    {
        // line 4
        echo "\t<a class=\"titulillo\">LISTADO DE RECETAS</a>
\t<br />
\tEstas son las recetas que están ahora mismo en la base de datos
";
    }

    // line 9
    public function block_contenido($context, array $blocks = array())
    {
        // line 10
        echo "\t<p>Listado de recetas</p>
\t
\t";
        // line 12
        if ($this->getContext($context, 'recetas')) {
            // line 13
            echo "\t
\t\t";
            // line 14
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, 'recetas'));
            foreach ($context['_seq'] as $context['_key'] => $context['receta']) {
                // line 15
                echo "\t\t
\t\t<p class=\"reclist\">";
                // line 16
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, 'receta'), "titulo", array(), "any", false), "html");
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, 'receta'), "usuario", array(), "any", false), "html");
                echo "</p>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['receta'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 18
            echo "\t";
        } else {
            // line 19
            echo "\t\t<p class=\"reclist\">No Hay recetas</p>
\t";
        }
        // line 21
        echo "
\t
\t
";
    }

    public function getTemplateName()
    {
        return "listado.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }
}
