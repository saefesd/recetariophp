<?php

/* login.twig.html */
class __TwigTemplate_2a21ce1492046b1874e21606fc2190d0 extends Twig_Template
{
    protected $parent;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'subtitulo' => array($this, 'block_subtitulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    public function getParent(array $context)
    {
        if (null === $this->parent) {
            $this->parent = $this->env->loadTemplate("layout.twig.html");
        }

        return $this->parent;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_subtitulo($context, array $blocks = array())
    {
        // line 4
        echo "\t<a class=\"titulillo\">LOGIN</a>
\t<br />
\tRellena los datos correctamente para poder loguearte
";
    }

    // line 9
    public function block_contenido($context, array $blocks = array())
    {
        // line 10
        echo "\t<div class=\"usunew\" align=\"center\">
\t\t<p class=\"tit2\">RELLENA LOS DATOS DE TU USUARIO</p>
\t\t<p>Nombre : <input id=\"name\" name=\"name\" type=\"text\" /></p>
\t\t<p>Contraseña : <input id=\"pass\" name=\"pass\" type=\"password\" /></p>
\t\t<br />
\t\t<input type=\"submit\" value=\"Limpiar\" name=\"limpiar\"id=\"limpiar\"><input type=\"submit\" value=\"Aceptar\" name=\"aceptar\" id=\"aceptar\">
\t</div>
\t
";
    }

    public function getTemplateName()
    {
        return "login.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }
}
