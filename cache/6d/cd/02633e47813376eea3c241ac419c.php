<?php

/* comentar.twig.html */
class __TwigTemplate_6dcd02633e47813376eea3c241ac419c extends Twig_Template
{
    protected $parent;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'subtitulo' => array($this, 'block_subtitulo'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    public function getParent(array $context)
    {
        if (null === $this->parent) {
            $this->parent = $this->env->loadTemplate("layout.twig.html");
        }

        return $this->parent;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_subtitulo($context, array $blocks = array())
    {
        // line 4
        echo "\t<a class=\"titulillo\">COMENTAR RECETA</a>
\t<br />
\tFormulario que recoge comentarios en referencia hacia una receta
";
    }

    // line 9
    public function block_contenido($context, array $blocks = array())
    {
        // line 10
        echo "\t<div class=\"usunew\" align=\"center\">
\t<p class=\"tit2\">RELLENA LOS DATOS DEL NUEVO COMENTARIO</p>
\t<p>Receta : <select name=\"recetas\">
\t\t<option value=\"\"> </option>
\t</select></p>
\t<p>Usuario:<input type='text' name ='usuario' disabled/></p>
\t\t<p>Comentario: <input type='text' id=\"icom\" name=\"comentario\"/> </p>
\t<input type='submit' value='Confirmar' id=\"iconf\"/>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "comentar.twig.html";
    }

    public function isTraitable()
    {
        return false;
    }
}
