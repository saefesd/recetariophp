<?php

require_once 'Twig/Autoloader.php';

Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('templates');

$twig = new Twig_Environment($loader, array(
			'cache' => 'cache',
			'debug' => 'true'));
			
$template = $twig->loadTemplate('listado.twig.html');
//echo $template->render(array());

	try{
			$conn = new PDO('sqlite:recetas.db');
			$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  // ESTO DARÁ MÁS INFOR
			$consulta = "SELECT * FROM principal_receta";
			$resultado = $conn -> query($consulta);   ### CUIDADO, ESTO NO ES EL RESULTADO
 			# configuramos el modo del fetch 
         $resultado->setFetchMode(PDO::FETCH_ASSOC);
			$recetas = $resultado->fetchall();
						
			$conn = null;
		}catch(PDOException $e ){
			echo $e -> getMessage();
		}
	
		
echo $template->render(array("recetas" => $recetas));
/*try{
			$conn = new PDO('sqlite:recetas.db');
			$consulta = "SELECT * FROM principal_recetas";
			$resultado = $conn -> query($consulta);
			
			
			foreach ($resultado as $receta) {
						$recetas = array($receta['titulo'],$receta['tipo'],$receta['ingredientes'],
						$receta['preparacion'], $receta['tiempo_registro'],$receta['usuario'],
						$receta['imagen'],);
			}
			
			$conn = null;
		}catch(PDOException $e ){
			echo $e -> getMessage();
		}
*/

/*
try{
$conexion = sqlite_open('recetas.db') or die ("No se ha podio establecer conexion");
$consulta = 'select * from recetas order by titulo';
$resultado = sqlite_exec($conexion,$consulta);

sqlite_close('recetas.db');
	$conexion = null;
		}catch(PDOException $e ){
			echo $e -> getMessage();
		}
echo $template->render(array($resultado));*/

//class MiBD extends SQLite3
//{
  //  function __construct()
    //{
     //   $this->open('recetas.db');
    //}
//}

//$bd = new MiBD();
//$resultado = $bd->query('SELECT * FROM recetas');
//var_dump($resultado->fetchArray());

// Base de datos

//$database = 'sqlite:database.db';
//$cn = $database(sqlite_open('recetas.db'));